---
title: "Liteserver"
slug: "Liteserver"
date: 2022-04-27T18:22:30+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)

- 网站：[直达链接](http://redirect.zhelper.net/?origin=liteserver)

### 评语

[@VPS_spider](https://t.me/VPS_spiders)

大流量 #大盘鸡 ，网络不错，流量一个月10T，超限的限流10MB/s，有该类需求的，买 #buyvm 或这个，支持支付宝付款。
(测试窥镜 (https://lg-dro.liteserver.nl/))(不抗投诉)
(请勿使用 VPN 或 TOR 订购 ，否则订单自动取消)
