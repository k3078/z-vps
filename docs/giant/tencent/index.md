---
title: "Tencent"
slug: "Tencent"
date: 2022-04-29T09:23:16+08:00
draft: false
---

[![SHIMIN](https://img.shields.io/badge/%E5%AE%9E%E5%90%8D-need-red)](https://vps.zhelper.net/intro) [![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro) [![FREE](https://img.shields.io/badge/free-tier-brightgreen)](https://free.zhelper.net/freevps/)

### 简介

- 腾讯云，良心云

### 参考价格

[3年轻量2H2G30M——230元 香港东京新加坡](https://t.me/wearemjj/672)  22/3 走代理，已失效

### 评价

[@kerm](https://qqvps.com/u/kerm)

腾讯云活动特别多，平时买了就亏了。最好是等到年初、双十一、618这种时候，或者走代理商。

默认要实名，另外买国内机器需要注意备案问题。**用于上网、挖矿后果自负。**