---
title: "Azure"
slug: "Azure"
date: 2022-04-29T09:23:50+08:00
draft: false
---

[![FREE](https://img.shields.io/badge/free-tier-brightgreen)](https://free.zhelper.net/freevps/)

### 简介

- Azure 微软云


### 评价

[@kerm](https://qqvps.com/u/kerm)

价格，日常来看还是有些昂贵。不过有免费服务。

通常所说 AZ100 是学生认证后无需信用卡即可获得的免费额度，一年 [Free tier](https://free.zhelper.net/freevps/)。AZ200 是信用卡认证后的免费额度。

Azure流量收费，绑卡后请注意这一点，不要把房子垫进去了。