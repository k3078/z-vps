---
title: "Digitalocean"
slug: "Digitalocean"
date: 2022-04-29T09:53:01+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![FREE](https://img.shields.io/badge/free-tier-brightgreen)](https://free.zhelper.net/freevps/) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)

### 简介

- Digitalocean DO

### 参考价格

Digital Ocean 1C1G VPS $5/month

### 评价

[@kerm](https://qqvps.com/u/kerm)

建议用GitHub教育包中的优惠码+绑卡购买，有一年 [Free tier](https://free.zhelper.net/freevps/)。性能不错，到中国网速慢。单绑卡也也有2个月 [Free tier](https://free.zhelper.net/freevps/)


