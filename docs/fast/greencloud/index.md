---
title: "Greencloud"
slug: "Greencloud"
date: 2022-04-27T18:31:52+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)

- greencloudvps  绿云
- 网站：[直达链接](http://redirect.zhelper.net/?origin=greencloud)
- 优惠码： (部分6折)OQDH735XPI (部分5折)YMBOX7JC3F


### 评价

[@kerm](https://qqvps.com/u/kerm)

比较火的一个商家，曾提供东京2222、5555配置的机型，Hostloc上有很多关于绿云的交易贴

----------------

[@VPS_spider](https://t.me/VPS_spiders)

黑五值得一看，网络还行。
机子配置性能限制严重，和 #virmach 有得一拼。
可能会出现高负载自动停机的现象。
(经常预售，得等一会才能开机)
(日常没啥优惠，看看就好)
