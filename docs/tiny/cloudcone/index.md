---
title: "Cloudcone"
slug: "Cloudcone"
date: 2022-04-27T18:36:54+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)

- 网站：[直达链接](http://redirect.zhelper.net/?origin=cloudcone)


### 参考价格

22/4 复活节

[$14.28年 2C 1GB 45GB 4TB 1Gbps 美国洛杉矶](https://hostloc.com/thread-1000375-1-1.html) 缺货

22/1 新年

[$9.99年 1C 0.5GB 20GB 2TB 1Gb 美国洛杉矶](https://hostloc.com/thread-988780-2-1.html) 缺货



### 评价

[@kerm](https://qqvps.com/u/kerm)

盘还比较大，也比较良心，之前想买个来做下载机。注册后就会定时收到他们家的优惠消息。

----------------

[@VPS_spider](https://t.me/VPS_spiders)

建站性价比有一点(黑五)，便宜带ddos防护。

黑五值得一看，日常非特价不推荐入，有点贵。

性能较拉跨，只适合轻量站点。

当然你只拿来搞代理那啥没什么问题。

对性能有要求建站还不如 #racknerd 。

部分机房和 #racknerd 同机房。

(ps：年付高于11刀，只是为了网络需求的，不值得买，详见评论区截图)
