---
title: "Alexhost"
slug: "Alexhost"
date: 2022-04-28T23:28:06+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/) [![BTC](https://img.shields.io/badge/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81-support-yellowgreen)](https://vps.zhelper.net/intro) 


- 网站：[直达链接](http://redirect.zhelper.net/?origin=alexhost)
- 优惠码：(-2%)B4IXURD5WEVR

### 参考价格

[AlexHost：€11.8/年/1.5GB内存/10GB SSD空间](https://hostloc.com/thread-632168-1-1.html)

### 评语

[@kerm](https://qqvps.com/u/kerm)

看了下价格还是可以的。不过[线路就一般般了](https://hostloc.com/thread-930174-1-1.html)

-------------

[@VPS_spider](https://t.me/VPS_spiders)

摩尔多瓦商家，官网宣传自己抗DMCA，#不限流量 ，年付最低$12.49，月付最低 $ 4.20 。联通电信移动都绕，IO大概300M/s，有时候会更低，速度很慢，延迟较高。
(窥镜:跳转 (https://lg.alexhost.com/))
