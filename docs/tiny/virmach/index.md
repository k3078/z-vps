---
title: "Virmach"
slug: "Virmach"
date: 2022-04-27T18:34:13+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/) [![LET](https://img.shields.io/badge/LET-have-blue)](https://lowendtalk.com/profile/VirMach)

- 官网：[直达链接](http://redirect.zhelper.net/?origin=virmach)
- 优惠码：(九折)SAVE10，(内存512MB以上) LEB30，(15%优惠) WHT15，(按流量优惠20%)，4T120EB20  ，2T120EB20，1T80EB20，500G50EB20

### 参考价格

22/3

[$8.88/年 1C 384MB 10G 1T japan](https://lowendtalk.com/discussion/177704/virmach-ryzen-nvme-8-88-yr-384mb-21-85-yr-2-5gb-instant-japan-pre-order-more/p1) 售罄

21/12

[$4.95/年 1C 0.5GB 10G 1000G BUFFALO, NY](https://t.me/hostloc2tg/280401) 售罄


### 个人评价

[@kerm](https://qqvps.com/u/kerm)

超售+延迟开机+区别对待。可以上黑名单的商家。买过他家的东京，延迟了15天才开机，之后也是各种随时宕机，搭个梯子都用得不爽。

不建议平时买，太贵，有折扣再上。

---------------

[@VPS_spider](https://t.me/VPS_spiders)

低端机狂魔，CPU超售2倍以上，内存超售1倍以上。

各种机器配置限定使用率，负载高不到5分钟会掉线。

只推荐买其中的便宜机子或者网络线路好的机子。

黑五或者特价促销值得一看，工单回复很慢。

(简单的说，机子只能跑网络，高IO或者CPU占用会被警告，多次警告后就删鸡，不退款，开机跳票是常态，买了能不能开机多久才开机看老板的意思)
