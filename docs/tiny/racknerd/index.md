---
title: "Racknerd"
slug: "Racknerd"
date: 2022-04-27T18:41:39+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![AFF](https://img.shields.io/badge/LET-have-blue)](https://lowendtalk.com/profile/dustinc) [![AFF](https://img.shields.io/badge/TG-channel-blue)](https://t.me/racknerd_promo) [![AFF](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro) [![AFF](https://img.shields.io/badge/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81-support-yellowgreen)](https://vps.zhelper.net/intro)

- 网站：[直达链接](http://redirect.zhelper.net/?origin=racknerd)
- 优惠码：(非特价机) 15OFFDEDI

### 参考价格

>这一家的机子种类非常多，很难收集全，这里只是贴几个有代表性的。

22/4 劳动节

[$15.58年 2C 2G 35G 5T 含测评](https://hostloc.com/thread-1005120-1-1.html)

[$9.99/年 1C 1G 25G 4T SanJose 含测评](https://hostloc.com/thread-1004458-1-1.html)

### 个人评价：

[@kerm](https://qqvps.com/u/kerm)

官网所提供的计划通常不是最优选择。如要购买，**推荐等到打折再买**。廉价鸡，LET Best Provider。平时喜欢没事就在LET送些小鸡。东西确实便宜，也算有性价比。买的话不要买圣何塞机房的。

有官方TG频道，可以加，就不会错过打折了。

之前劳动节买过一个9.9，加hysteria蛮不错。

均可双倍流量。

-----------

[@VPS_spider](https://t.me/VPS_spiders)

日常性价比高且便宜。

每周起码掉线几次且部分地区延迟高，不影响使用。

推荐日常代理使用(网络要求一般那种)或套cf建站的。

网络还是可以的，黑五值得一看。

工单回复极快，部分机房和 #cloudcone 同机房，但性能较之 #cloudcone 好多了。

性能没有约束很厉害，短时间高负载不会停机。(比 virmach 好多了)

(别dd win系统，违反tos的会被停机)


(窥镜：官方测试IP (https://lg-lax02.racknerd.com/))(推荐洛杉矶，电信直连)

备注：

历年劳动节的机子买了，评论区留言双倍流量：[LET](https://lowendtalk.com/discussion/178275/its-over-heres-this-dont-forget#latest)

历年黑色星期五和新年的机子买了，评论区留言双倍流量：[LET](https://lowendtalk.com/discussion/178569/alphavps-dedicated-server-spring-mega-sale-ryzen-vps-launch-amd-epyc-kvm-sale#latest)

(LET账号注册后不显示留言框等大半天就有了，需要后台人员审核过后才能留言评论)
