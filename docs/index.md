---
title: "关于本手册"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

## 评论模块加载较慢，请等待加载完全！！！！

## 简介

本部分是 Zhelper 项目的一部分。

如果您希望参与到本文档的编辑中，或是对教程有不理解的地方，可以加入 [TG群](https://t.me/zhelper)（国内无法直接访问） 或 [论坛](https://bbs.zhelper.net/)


<!-- ## 关于我们

本文档的形成得益于许多人的努力。

这里特别感谢：

1. [VPS补货通知频道](https://t.me/VPS_spiders) 作者大佬。 VPS_spiders提供20多个主机服务商库存实时监控，可以帮助您第一时间抢到想要的主机。 -->


<!-- 2. [QQVPS论坛](https://qqvps.com/fourm/) 站长大佬。 QQVPS论坛无需邀请即可注册，是我们讨论VPS最佳场所之一。 -->

<!-- ## reference

1. 主机搜索、测评：[Server Hunter - Find a server](https://www.serverhunter.com/)
2. 测评：https://t.me/VPS_spiders
3. 名词解释：[VPS黑话科普：杜甫/小鸡/大盘鸡/MJJ/吃灰/传家宝|全球VPS交流社区 (qqvps.com)](https://qqvps.com/d/580) -->
