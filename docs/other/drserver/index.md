---
title: "Drserver"
slug: "Drserver"
date: 2022-04-28T23:14:07+08:00
draft: false
---


[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/) [![LES](https://img.shields.io/badge/LES-%E6%9C%89-blue)](https://lowendspirit.com/profile/Radi)


- 网站：[直达链接](http://redirect.zhelper.net/?origin=drserver)


### 参考价格

>[100刀年付 Left Intel® Atom™ C2750 (8-Core, 2.4GHz) 8GB DDR3 1x 2TB SATA](https://t.me/hostloc2tg/33645)

### 评语


[@kerm](https://qqvps.com/u/kerm)

简单看了下，似乎有一款年付100$独服比较火。没用过。

--------------

[@VPS_spider](https://t.me/VPS_spiders)

老牌低端机商家，最近没啥优惠活动，看看就好。
首次服务提供 30 天退款政策，下单不允许挂代理。
线路自测。
(窥镜：IPV4 (http://lg-dal.ipv4.drserver.net/))
(窥镜：IPV6 (http://lg-dal.ipv6.drserver.net/))
