---
title: "Kts24"
slug: "Kts24"
date: 2022-04-28T23:09:10+08:00
draft: false
---

[![NAFF](https://img.shields.io/badge/AFF-%E6%97%A0-green)](https://vps.zhelper.net/intro/#aff) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/) [![LES](https://img.shields.io/badge/LES-%E6%9C%89-blue)](https://lowendspirit.com/profile/RapToN)


- 网站：[直达链接](https://www.kts24.com/vps)
- 最低充值5欧

### 参考价格

[0.29€/月 1C 0.5 GB 5 GB硬盘 100 NAT4 IPv6](https://hostloc.com/thread-971573-1-1.html)

### 评语

[@VPS_spider](https://t.me/VPS_spiders)

低端欧洲鸡商家，稳定性还行，CPU拉跨，延迟较高，地区NATKVM德国，VPSKVM德国，好像有波兰可选，年付有优惠，1小时内创建删鸡不扣款，余额原额返还。
(面板比较专业化，安装系统比较麻烦，NAT我装系统得快1小时。。。)
日常最低价的KVM的VPS能到1.2~1.5欧一个月。
(给的IP也奇葩，1个IPV4+10个IPV6是标配，NAT也有10个IPV6和1个IPV4的22+1端口)
(默认不会加载IPV6配置，需要加载记得面板设置)
NAT的KVM的鸡更便宜，0.39欧~0.69欧一个月。
速度还行，吃灰鸡+1，探针+1，可以拿来当玩具。
(注意这家得自己装ISO，在ISOManager中选择Debian10，然后关机再开机，自动加载ISO进行安装，剩下的你得会安装debian10才行，安装过程中千万别重启，否则硬盘会读取不到，出现这种情况你只能发工单问客服了）
(还有NAT的安装很慢，耐心等待吧，安装的最后一步是重启加载新系统，这时候记得在ISOManager中把debian10镜像卸下来，避免重启又开始重新安装了)
(为什么是debian10？因为这个镜像是除了apline的占用最小的镜像)
(流量也别跑太狠了，1个月跑10T那种会被清退，日常跑跑500G以内是稳的)
(这家充值不收手续费，但也别指望他做生产环境，只拿来当代理用鸡是不错的玩具)
