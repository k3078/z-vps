---
title: "Webhorizon"
slug: "Webhorizon"
date: 2022-04-27T18:26:23+08:00
draft: false
---

[![NAFF](https://img.shields.io/badge/AFF-%E6%97%A0-green)](https://vps.zhelper.net/intro/#aff) [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/) [![BTC](https://img.shields.io/badge/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81-support-yellowgreen)](https://vps.zhelper.net/intro)

- 网站：[webhorizon](https://my.webhorizon.in/) ，[cloudV](https://cloud-v.net/)

### 参考价格

21/11 闪购

[$2.5/年 1C 256MB 4GB 500GB /80 IPv6 + NAT IPv4 荷兰](https://hostloc.com/thread-928657-1-1.html) 售罄

21/5 

[$7/年 1C 256MB 4GB 500GB /80 IPv6 + NAT IPv4 香港 附测试](https://hostloc.com/thread-838990-1-1.html) 售罄


### 评价

[@kerm](https://qqvps.com/u/kerm)

印度商家。有香港低价NAT机，线路一般，据说有[被墙](https://www.hostloc.com/thread-855029-1-1.html)。

[有把AFF干掉的历史](https://hostloc.com/thread-988470-1-1.html) 

-------

[@VPS_spider](https://t.me/VPS_spiders)

cloudV 和 webhorizon 是同一个所有者的不同品牌, #webhorizon 太便宜了被国人玩烂了IP，大部分IP是被墙的，套cf都没法救回来。

cloudV是新子品牌，机子不是相同系统里的，没被墙，但也不便宜。

极其不推荐，**转移账户下的服务器也是得给钱的**。
