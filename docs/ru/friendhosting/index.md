---
title: "Friendhosting"
slug: "Friendhosting"
date: 2022-04-27T18:14:01+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff)  [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro)  [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)  [![BTC](https://img.shields.io/badge/%E5%8A%A0%E5%AF%86%E8%B4%A7%E5%B8%81-support-yellowgreen)](https://vps.zhelper.net/intro)

- 网站：[直达链接](http://redirect.zhelper.net/?origin=friendhosting)
- 优惠码：(50%一次性，最长买一年)13YEAR

### 参考价格

[1核 512M 5G €16/年](https://www.daniao.org/16119.html) 活动

[$28.74/yr 2GB KVM VPS in Poland / Switzerland](https://www.lowendtalk.com/discussion/175211/2021-black-friday-cyber-monday-official-megathread-flash-deals/p67) 活动
 
### 评价

[@kerm](https://qqvps.com/u/kerm)

俄罗斯低价VPS商家。特点是对国内支付方式支持很好，不限流量。

------------

[@VPS_spider](https://t.me/VPS_spiders)

欧洲老商家，保加利亚、波兰、捷克共和国、拉脱维亚、瑞士、荷兰、美国洛杉矶、美国迈阿密、乌克兰哈尔科夫、乌克兰基辅多个位置可选。
**不限流量**
线路自测，洛杉矶应该是最快的。
(窥镜：[跳转](https://friendhosting.net/looking-glass.php))
