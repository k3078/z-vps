---
title: "Hostvds"
slug: "Hostvds"
date: 2022-04-27T18:27:26+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff)  [![ZFB](https://img.shields.io/badge/%E6%94%AF%E4%BB%98%E5%AE%9D%2F%E5%BE%AE%E4%BF%A1-support-green)](https://vps.zhelper.net/intro)  [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)

- 网站：[直达链接](http://redirect.zhelper.net/?origin=hostvds)


### 参考价格

[$0.79每月 1 CPU 1 GB  50 Mbps 10 GB SSD](https://hostloc.com/thread-862282-1-1.html) 长期

### 评价

[@kerm](https://qqvps.com/u/kerm)

没用过，

>[贴个hostloc评价](https://hostloc.com/thread-851161-1-1.html)：
买了一个0.79&一个月的小鸡鸡，用两天莫名其妙进不去，一ping不通，官网去看没什么异常客服两天不理人，果然太垃圾了，要不是我穷哦。

---------------

[@VPS_spider](https://t.me/VPS_spiders)

垃圾玩具鸡一个，线路全球绕，7天有3天ping不通。

客服几天不回复。

除了最便宜(以月为单位买的话)，没别的优点，全是缺点。

当 #玩具鸡 玩玩还行。#垃圾鸡
