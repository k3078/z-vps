---
title: "Ruvds"
slug: "Ruvds"
date: 2022-04-27T18:35:36+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff) 

- 官网：[直达链接](http://redirect.zhelper.net/?origin=ruvds)
- 优惠码 cheap30

### 评价

[@kerm](https://qqvps.com/u/kerm)

所谓的3毛机就是月付3元毛子机，4毛5毛同理。3毛常年缺货，平时能买到的最便宜配置为1vCPU/0.5G RAM/10G硬盘/100M带宽/不限制流量，需要手动调节配置和应用优惠码，参考[本文](https://dev.kermsite.com/p/ruvds%E5%B9%B4%E4%BB%98/)

>[贴一个hostloc上的测评](https://hostloc.com/thread-950428-1-1.html)

--------------

[@VPS_spider](https://t.me/VPS_spiders)

虽然便宜，但网络一般还是俄区，未来有涨价趋势。

其中的30卢布和50卢布一个月的鸡值得一入，性价比很高，暂时缺货。

虽然说是性价比高，但是上述套餐属于低配套餐，io比较拉跨，不推荐建站用。
