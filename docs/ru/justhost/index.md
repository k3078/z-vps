---
title: "Justhost"
slug: "Justhost"
date: 2022-04-27T17:55:06+08:00
draft: false
---

[![AFF](https://img.shields.io/badge/AFF-%E6%9C%89-red)](https://vps.zhelper.net/intro/#aff)   [![Paypal](https://img.shields.io/badge/Paypal-support-blue)](https://vps.zhelper.net/intro/)


- 官网：[直达链接](http://redirect.zhelper.net/?origin=justhost)


### 参考价格

[10Mb 月付不到5元](https://hostloc.com/thread-974071-1-1.html)  （已涨价）

### 评价

[@kerm](https://qqvps.com/u/kerm)

俄罗斯低价VPS商家，

最低年付六美元，带宽10M，不限流量。可以挂P2P尝试回本。

直达：https://justhost.ru/services/vps/?tariff=promo#changeconfig

有一台最低配置年付机。挂探针服务器和P2P。基本零成本。放低期望，勉强能用。

不适合做梯子。速度还没有Heroku快。

![](2022-04-28-18-23-23.png)

---------------------

[@VPS_spider](https://t.me/VPS_spiders)

比垃圾好一点的 #垃圾鸡，这玩意别买就对了。

IO拉跨石头盘，网络掉线严重。

各种配置高占用是直接停机。

以前还能说有性价比，现在不是贪图俄区IP没必要买。

就算需要毛子IP也有别的好网络。

现在它的配置和表现根本不值那个价，只推荐其中的 #玩具鸡 玩玩